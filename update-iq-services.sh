#!/bin/bash

WORKSPACE=/home/jguo/dds-workspace
# SERVICES=(booking )
SERVICES=(booking address rider-management transit-driver transit-vehicle transit-dispatch scheduling iq-import-adapter continuous-optimization)
SERVICEDIR=
FAILED_TO_UPDATE=()

# append -service to the services-name provided
get_service_dirname() {
    if [ $1 ]; then
        SERVICEDIR="$WORKSPACE/$1-service"
    else
        echo "invalid input in get_service_dirname, expecting a string"
        exit 1
    fi
    return 0
}

git_update() {
    # double check matching service name vs the current directory
    var=$(pwd)

    if [[ $var != *"$1"* ]]; then
        echo "ERROR: current directory @ $var does not match service $1"
    else
        git checkout new-domain-model
        git pull

        if [ $? -ne 0 ]; then
            return 1
        fi
    fi

    return 0
}

yarn_install() {
    yarn install

    if [ $? -ne 0 ]; then
        echo "ERROR: error encountered during yarn install."
	return 1
    fi

    return 0
}
update_services() {
    arr=("$@")

    cd $WORKSPACE
    if [ $? -ne 0 ]; then
        echo "ERROR: Invalid WORKSPACE."
    fi

    echo "INFO: Entered workspace @ $WORKSPACE"

    if [ ${#arr[@]} -gt 0 ]; then
        SERVICES=("$@")
    else
        echo "INFO: No overriding Services, updating default service list: ${SERVICES[@]}"
    fi

    for service in "${SERVICES[@]}"
    do
        echo -e "\n======================\n$service\n======================"
        # clear variables used
        SERVICEDIR=

        get_service_dirname $service

        if [ $? -ne 0 ]; then
            echo "$SERVICEDIR : update failed ..."
        else
            cd $SERVICEDIR
            if [ $? -ne 0 ]; then
                echo "ERROR: Invalid service Directory. update failed ..."
                FAILED_TO_UPDATE+=($service)
                return 1
            fi

            echo "INFO: Entered service directory @ $SERVICEDIR"

            git_update $service
            if [ $? -eq 0 ]; then
                echo "INFO: $service service update success"
            else
                FAILED_TO_UPDATE+=($service)
            fi

	    yarn_install
	    if [ $? -eq 0 ]; then
                echo "INFO: $service service initialization ...  success"
            fi
        fi
    done


}

update_services "${SERVICES[@]}"
echo -e "\n======================\nWARN: The following services failed to update: ${FAILED_TO_UPDATE[@]}\n======================\n"



